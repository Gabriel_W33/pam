package com.example.pam.pamg.feature;

public class Question {

    private int TextResId;
    private boolean answerTrue;


    public Question(int TextResId, boolean answerTrue) {
        this.TextResId = TextResId;
        this.answerTrue = answerTrue;

    }


    public int getTextResId() {
        return TextResId;
    }

    public void setTextResId(int TextResId) {
        this.TextResId = TextResId;
    }

    public boolean isAnswerTrue() {
        return answerTrue;
    }

    public void setAnswerTrue(boolean answerTrue) {
        this.answerTrue = answerTrue;
    }




}