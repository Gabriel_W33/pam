package com.example.pam.pamg.feature;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {


    private static final String TAG = "MainActivity";
    private static final String INDEX = "index";
    private static final String POINTS = "points";
    private static final String TOKENS = "tokens";
    private static final String CHEAT_USED = "cheat_used";
    private static final String ANSWER_ARRAY = "answerArray";
    private static final String WHAT_CHOSE = "WhatChose";


    private boolean[] IsAnswer = new boolean[mQuestionBank.length];
    private boolean[] IsCheat = new boolean[mQuestionBank.length];
    private int[] WhatChose = new int[mQuestionBank.length];


    private TextView mQuestionTextView;
    private ImageButton mTrueButton;
    private ImageButton mFalseButton;
    private ImageButton mBackButton;
    private TextView mPointCountView;
    private TextView mTokenView;
    private TextView mBazaView;
    @SuppressLint("StaticFieldLeak")
    static ImageButton mCheatButton;
    private TextView mApiView;
    private static int mCurrentIndex = 0;
    int mPointCount = 0;
    private int mQuestionCounter = 0;
    int tokens ;
    int StartTokens =3;
    int ExtraCheatCanter=0;



    private static Question[] mQuestionBank = new Question[]{
            new Question(R.string.question_stolica_polski, true),
            new Question(R.string.question_stolica_dolnego_slaska, false),
            new Question(R.string.question_sniezka, true),
            new Question(R.string.question_wisla, true),
    };




    public void AlreadyAnswerTrue() {
        IsAnswer[mCurrentIndex] = true;
        Log.d(TAG, "AlreadyAnswerTrue: Index " + mCurrentIndex + " Is Ans" + IsAnswer[mCurrentIndex]);
    }


    private void NextQuestion() {
        if (mCurrentIndex < mQuestionBank.length - 1) {
            mCurrentIndex += 1;
            updateQuestion();
        } else {
            updateQuestion();
        }
    }

    private void previousQuestion() {
        if (mCurrentIndex > 0) {
            mCurrentIndex -= 1;
            updateQuestion();
        }
    }

    public void OpenCheatActivity() {

        Log.d(TAG, "onClick: ");
        Intent intent = new Intent(this, CheatActivity.class);
        boolean isTrue = mQuestionBank[mCurrentIndex].isAnswerTrue();
        intent.putExtra(CheatActivity.EXTRA_ANSWER_IS_TRUE, isTrue);

        startActivityForResult(intent, 0);
    }

    private void UpdateToken(){

            tokens = StartTokens - ExtraCheatCanter ;
            String Ts = Integer.toString(tokens);
            mTokenView.setText(Ts);

    }


    public void checkAnswer(boolean ifClickTrue) {

        boolean isTrue = mQuestionBank[mCurrentIndex].isAnswerTrue();
        int messageResId;

        if (!IsAnswer[mCurrentIndex]) {
            if (ifClickTrue == isTrue) {
                messageResId = R.string.correct;
                mPointCount += 1;
                AlreadyAnswerTrue();
            } else {
                messageResId = R.string.incorrect;
                AlreadyAnswerTrue();
            }
            mQuestionCounter += 1;

            Toast toast = Toast.makeText(this, messageResId, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 0, 20);
            toast.show();
            updateQuestion();
            NextQuestion();
        }

    }

    public void changeColorCheatButton() {
        if (tokens > 0) {
//            if (IsCheat[mCurrentIndex]) {
//                mCheatButton.setBackgroundColor(Color.RED);
//            } else {
//                mCheatButton.setBackgroundColor(Color.GREEN);
//            }
        }else {
            mCheatButton.setVisibility(View.INVISIBLE);
        }
    }

    private void changeColorAnswerButton() {
        if (IsAnswer[mCurrentIndex]) {
            if (mQuestionBank[mCurrentIndex].isAnswerTrue()) {
                if (WhatChose[mCurrentIndex] == 2) {
                    mTrueButton.setBackgroundColor(Color.GREEN);
                    mFalseButton.setBackgroundColor(Color.GRAY);
                } else {
                    mFalseButton.setBackgroundColor(Color.RED);
                    mTrueButton.setBackgroundColor(Color.GRAY);
                }
            } else if (WhatChose[mCurrentIndex] == 1) {
                mFalseButton.setBackgroundColor(Color.GREEN);
                mTrueButton.setBackgroundColor(Color.GRAY);
            } else {
                mTrueButton.setBackgroundColor(Color.RED);
                mFalseButton.setBackgroundColor(Color.GRAY);
            }
        } else {
            mFalseButton.setBackgroundColor(Color.GRAY);
            mTrueButton.setBackgroundColor(Color.GRAY);
        }

    }

    private void WhatWasChosenAnswerButton(int IndexChosen) {
        if (!IsAnswer[mCurrentIndex]) {
            WhatChose[mCurrentIndex] = IndexChosen;
        }
    }


    public void updateQuestion() {
        UpdateToken();
        int quest = mQuestionBank[mCurrentIndex].getTextResId();
        mQuestionTextView.setText(quest);
        changeColorCheatButton();
        changeColorAnswerButton();


        if (mQuestionCounter == mQuestionBank.length) {
            String point = (Integer.toString(mPointCount) + " / " + Integer.toString(mQuestionBank.length));
            mPointCountView.setText(point);

        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent cheatIntent) {
        if (cheatIntent != null) {
            ExtraCheatCanter += cheatIntent.getIntExtra(CheatActivity.EXTRA_CHEAT_COUNTER,0);
            UpdateToken();
            changeColorCheatButton();
        }

    }

        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mQuestionTextView = findViewById(R.id.Question);
        mTokenView = findViewById(R.id.TokenTextView);
        mTrueButton = findViewById(R.id.TrueB);
        mFalseButton = findViewById(R.id.FalseB);
        mBackButton = findViewById(R.id.BackB);
        mPointCountView = findViewById(R.id.PointCount);
        mCheatButton = findViewById(R.id.CheatB);
        mBazaView = findViewById(R.id.BazaView);

        if (savedInstanceState != null) {
            mCurrentIndex = savedInstanceState.getInt(INDEX, 0);
            IsCheat = savedInstanceState.getBooleanArray(CHEAT_USED);
            mPointCount = savedInstanceState.getInt(POINTS);
            ExtraCheatCanter = savedInstanceState.getInt(TOKENS);
            IsAnswer = savedInstanceState.getBooleanArray(ANSWER_ARRAY);
            WhatChose = savedInstanceState.getIntArray(WHAT_CHOSE);
        }


        updateQuestion();

        mApiView = findViewById(R.id.ApiTextView);

        mApiView.setText(Integer.toString(Build.VERSION.SDK_INT));

        mCheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenCheatActivity();
            }
        });


        mTrueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WhatWasChosenAnswerButton(2);
                checkAnswer(true);
                updateQuestion();
            }

        });


        mFalseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WhatWasChosenAnswerButton(1);
                checkAnswer(false);
                updateQuestion();

            }

        });

        mQuestionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NextQuestion();
            }
        });


        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                previousQuestion();
            }
        });


    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mCurrentIndex = savedInstanceState.getInt(INDEX, 0);
        mPointCount = savedInstanceState.getInt(POINTS);
        ExtraCheatCanter = savedInstanceState.getInt(TOKENS);
        IsAnswer = savedInstanceState.getBooleanArray(ANSWER_ARRAY);
        IsCheat = savedInstanceState.getBooleanArray(CHEAT_USED);
        WhatChose = savedInstanceState.getIntArray(WHAT_CHOSE);


        super.onRestoreInstanceState(savedInstanceState);


    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(INDEX, mCurrentIndex);
        savedInstanceState.putInt(POINTS, mPointCount);
        savedInstanceState.putInt(TOKENS, ExtraCheatCanter);
        savedInstanceState.putBooleanArray(ANSWER_ARRAY, IsAnswer);
        savedInstanceState.putBooleanArray(CHEAT_USED, IsCheat);
        savedInstanceState.putIntArray(WHAT_CHOSE, WhatChose);
        super.onSaveInstanceState(savedInstanceState);
    }

}
