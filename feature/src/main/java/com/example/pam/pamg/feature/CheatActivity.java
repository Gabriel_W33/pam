package com.example.pam.pamg.feature;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;



public class CheatActivity extends AppCompatActivity {

    private static final String TAG = "CheatActivity";

    public static final String EXTRA_ANSWER_IS_TRUE = "CorrectAns";
    public static final String CHEATED = "Cheated";

    public static final String EXTRA_CHEAT_COUNTER = "CountCheat";



    private Button mCheatButton;
    private TextView mCheatView;
    private boolean isTrue;
    private boolean isCheated=false;

    private int cheatCounter;



    private void CheatAnswer(){

        if(isCheated){
            if(isTrue){
                mCheatView.setText("True");

            }else{
                mCheatView.setText("False");
            }
            mCheatButton.setVisibility(View.INVISIBLE);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chaet);


        mCheatButton = findViewById(R.id.CheatAnswer);
        mCheatView = findViewById(R.id.CheatText);
        isTrue = getIntent().getBooleanExtra(EXTRA_ANSWER_IS_TRUE,false);

        if (savedInstanceState != null){
             cheatCounter = savedInstanceState.getInt(EXTRA_CHEAT_COUNTER);
             isCheated = savedInstanceState.getBoolean(CHEATED);
        }
        Log.d(TAG, "cheat counter22 :" + cheatCounter);



        CheatAnswer();


        mCheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCheated =true;
                setAnswerCheatedResult();
                CheatAnswer();

            }
        });
    }


    private void setAnswerCheatedResult(){

        cheatCounter = 1;
        Intent cheatIntent = new Intent();
        cheatIntent.putExtra(EXTRA_CHEAT_COUNTER, cheatCounter);
        Log.d(TAG, "cheat counter:" + cheatCounter);
       setResult(RESULT_OK, cheatIntent);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(EXTRA_CHEAT_COUNTER, cheatCounter );
        savedInstanceState.putBoolean(CHEATED, isCheated );

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        cheatCounter = savedInstanceState.getInt(EXTRA_CHEAT_COUNTER);
       // Log.d(TAG, "cheat counter22 :" + cheatCounter);
        Intent cheatIntent = new Intent();
        cheatIntent.putExtra(EXTRA_CHEAT_COUNTER, cheatCounter);
        isCheated = savedInstanceState.getBoolean(CHEATED);
        setResult(RESULT_OK, cheatIntent);

    }


}


